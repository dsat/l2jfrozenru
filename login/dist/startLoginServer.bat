@echo off
title L2J-Frozen: Login Server Console
:start
echo ------------------------------
echo Starting LoginServer...
echo Website : www.l2jfrozen.com
echo ------------------------------
echo.

set CLASSPATH=%CLASSPATH%;./*;./config/bean/*;./lib/*;
set MAIN_CLASS=com.l2jfrozen.loginserver.Main
set JMX_REMOTE=-Dcom.sun.management.jmxremote.port=10998 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false

java -Xms128m -Xmx128m -cp %CLASSPATH% %MAIN_CLASS% %JMX_REMOTE% -Dfile.encoding=UTF8

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end
:restart
echo.
echo Admin Restarted ...
ping -n 5 localhost > nul
echo.
goto start
:error
echo.
echo LoginServer terminated abnormaly
ping -n 5 localhost > nul
echo.
goto start
:end
echo.
echo LoginServer terminated
echo.
:question
set choix=q
set /p choix=Restart(r) or Quit(q)
if /i %choix%==r goto start
if /i %choix%==q goto exit
:exit
exit
pause
