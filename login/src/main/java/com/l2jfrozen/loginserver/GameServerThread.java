package com.l2jfrozen.loginserver;

import com.l2jfrozen.configuration.LoginServerConfig;
import com.l2jfrozen.context.SpringApplicationContext;
import com.l2jfrozen.crypt.CryptManager;
import com.l2jfrozen.crypt.NewCrypt;
import com.l2jfrozen.database.GameServerInstanceManager;
import com.l2jfrozen.database.manager.GameServerManager;
import com.l2jfrozen.loginserver.network.gameserverpackets.*;
import com.l2jfrozen.loginserver.network.loginserverpackets.*;
import com.l2jfrozen.loginserver.network.serverpackets.ServerBasePacket;
import com.l2jfrozen.thread.GameServerInfo;
import com.l2jfrozen.thread.ServerThread;
import com.l2jfrozen.util.Util;
import javolution.util.FastSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyPair;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 * @author -Wooden-
 * @author KenM
 */
public class GameServerThread extends Thread implements ServerThread {
    protected static final Logger LOGGER = LoggerFactory.getLogger(GameServerThread.class.getName());
    private final Socket _connection;
    private InputStream _in;
    private OutputStream _out;
    private final RSAPublicKey _publicKey;
    private final RSAPrivateKey _privateKey;
    private NewCrypt _blowfish;
    private byte[] _blowfishKey;
    private final String _connectionIp;
    private GameServerInfo _gsi;
    /**
     * Authed Clients on a GameServer
     */
    private final Set<String> _accountsOnGameServer = new FastSet<>();
    private String _connectionIPAddress;

    public GameServerThread(Socket con) {
        _connection = con;
        _connectionIp = con.getInetAddress().getHostAddress();
        try {
            _in = _connection.getInputStream();
            _out = new BufferedOutputStream(_connection.getOutputStream());
        } catch (final IOException e) {
            LOGGER.warn("", e);
        }

        final KeyPair pair = CryptManager.getInstance().getKeyPair();
        _privateKey = (RSAPrivateKey) pair.getPrivate();
        _publicKey = (RSAPublicKey) pair.getPublic();
        _blowfish = new NewCrypt("_;v.]05-31!|+-%xT!^[$\00");
    }

    @Override
    public void run() {
        boolean checkTime = true;
        final long time = System.currentTimeMillis();
        _connectionIPAddress = _connection.getInetAddress().getHostAddress();

        final InitLS startPacket = new InitLS(_publicKey.getModulus().toByteArray());
        try {
            sendPacket(startPacket);

            int lengthHi;
            int lengthLo;
            int length = 0;
            boolean checksumOk;
            while (true) {
                if (time - System.currentTimeMillis() > 10000 && checkTime) {
                    _connection.close();
                    break;
                }

                try {
                    lengthLo = _in.read();
                    lengthHi = _in.read();
                    length = lengthHi * 256 + lengthLo;
                } catch (final IOException e) {
                    lengthHi = -1;
                }

                if (lengthHi < 0 || _connection.isClosed()) {
                    LOGGER.warn("LoginServerThread: Login terminated the connection.");
                    break;
                }

                byte[] data = new byte[length - 2];

                int receivedBytes = 0;
                int newBytes = 0;

                while (newBytes != -1 && receivedBytes < length - 2) {
                    newBytes = _in.read(data, 0, length - 2);
                    receivedBytes = receivedBytes + newBytes;
                }

                if (receivedBytes != length - 2) {
                    LOGGER.warn("Incomplete Packet is sent to the server, closing connection.(LS)");
                    break;
                }

                // decrypt if we have a key
                data = _blowfish.decrypt(data);
                checksumOk = NewCrypt.verifyChecksum(data);

                if (!checksumOk) {
                    LOGGER.warn("Incorrect packet checksum, closing connection (LS)");
                    return;
                }

                if (LoginServerConfig.DEBUG) {
                    LOGGER.warn("[C]\n" + Util.printData(data));
                }

                final int packetType = data[0] & 0xff;
                switch (packetType) {
                    case 0x00:
                        checkTime = false;
                        onReceiveBlowfishKey(data);
                        break;
                    case 0x01:
                        onGameServerAuth(data);
                        break;
                    case 0x02:
                        onReceivePlayerInGame(data);
                        break;
                    case 0x03:
                        onReceivePlayerLogOut(data);
                        break;
                    case 0x04:
                        onReceiveChangeAccessLevel(data);
                        break;
                    case 0x05:
                        onReceivePlayerAuthRequest(data);
                        break;
                    case 0x06:
                        onReceiveServerStatus(data);
                        break;
                    default:
                        LOGGER.warn("Unknown Opcode (" + Integer.toHexString(packetType).toUpperCase() + ") from GameServer, closing connection.");
                        forceClose(LoginServerFail.NOT_AUTHED);
                }

            }
        } catch (final IOException e) {
            if (LoginServerConfig.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();

            final String serverName = getServerId() != -1 ? "[" + getServerId() + "] " + GameServerManager.getInstance().getServerName(getServerId()) : "(" + _connectionIPAddress + ")";
            final String msg = "GameServer " + serverName + ": Connection lost: " + e.getMessage();
            LOGGER.info(msg);
        } finally {
            if (isAuthed()) {
                _gsi.setDown();
                LOGGER.info("Server [" + getServerId() + "] " + GameServerManager.getInstance().getServerName(getServerId()) + " is now set as disconnected");
            }
            // LoginServer loginServer= (LoginServer) SpringApplicationContext.getBean(LoginServer.class);
            // loginServer.getGameServerListener().removeGameServer(this);
            // loginServer.getGameServerListener().removeFloodProtection(_connectionIp);
            final GameServerListener gameServerListener = (GameServerListener) SpringApplicationContext.getBean(GameServerListener.class);
            gameServerListener.removeGameServer(this);
            gameServerListener.removeFloodProtection(_connectionIp);
        }
    }

    private void onReceiveBlowfishKey(byte[] data) {

        final BlowFishKey bfk = new BlowFishKey(data, _privateKey);
        _blowfishKey = bfk.getKey();
        _blowfish = new NewCrypt(_blowfishKey);

        if (LoginServerConfig.DEBUG) {
            LOGGER.info("New BlowFish key received, Blowfih Engine initialized:");
        }
    }

    private void onGameServerAuth(byte[] data) throws IOException {
        final GameServerAuth gsa = new GameServerAuth(data);

        if (LoginServerConfig.DEBUG) {
            LOGGER.info("Auth request received");
        }

        handleRegProcess(gsa);

        if (isAuthed()) {
            final AuthResponse ar = new AuthResponse(getGameServerInfo().getId());
            sendPacket(ar);

            if (LoginServerConfig.DEBUG) {
                LOGGER.info("Authed: id: " + getGameServerInfo().getId());
            }
        }
    }

    private void onReceivePlayerInGame(byte[] data) {
        if (isAuthed()) {
            final PlayerInGame pig = new PlayerInGame(data);
            final List<String> newAccounts = pig.getAccounts();

            for (final String account : newAccounts) {
                _accountsOnGameServer.add(account);

                if (LoginServerConfig.DEBUG) {
                    LOGGER.info("Account " + account + " logged in GameServer: [" + getServerId() + "] " + GameServerManager.getInstance().getServerName(getServerId()));
                }
            }

        } else {
            forceClose(LoginServerFail.NOT_AUTHED);
        }
    }

    private void onReceivePlayerLogOut(byte[] data) {
        if (isAuthed()) {
            final PlayerLogout plo = new PlayerLogout(data);
            _accountsOnGameServer.remove(plo.getAccount());

            if (LoginServerConfig.DEBUG) {
                LOGGER.info("Player " + plo.getAccount() + " logged out from gameserver [" + getServerId() + "] " + GameServerManager.getInstance().getServerName(getServerId()));
            }
        } else {
            forceClose(LoginServerFail.NOT_AUTHED);
        }
    }

    private void onReceiveChangeAccessLevel(byte[] data) {
        if (isAuthed()) {
            final ChangeAccessLevel cal = new ChangeAccessLevel(data);
            LoginController.setAccountAccessLevel(cal.getAccount(), cal.getLevel());
            LOGGER.info("Changed " + cal.getAccount() + " access level to " + cal.getLevel());
        } else {
            forceClose(LoginServerFail.NOT_AUTHED);
        }
    }

    private void onReceivePlayerAuthRequest(byte[] data) throws IOException {
        if (isAuthed()) {
            final PlayerAuthRequest par = new PlayerAuthRequest(data);
            PlayerAuthResponse authResponse;

            if (LoginServerConfig.DEBUG) {
                LOGGER.info("auth request received for Player " + par.getAccount());
            }

            final SessionKey key = LoginController.getKeyForAccount(par.getAccount());

            if (key != null && key.equals(par.getKey())) {
                if (LoginServerConfig.DEBUG) {
                    LOGGER.info("auth request: OK");
                }

                LoginController.removeAuthedLoginClient(par.getAccount());
                authResponse = new PlayerAuthResponse(par.getAccount(), true);
            } else {
                if (LoginServerConfig.DEBUG) {
                    LOGGER.info("auth request: NO");
                    LOGGER.info("session key from self: " + key);
                    LOGGER.info("session key sent: " + par.getKey());
                }
                authResponse = new PlayerAuthResponse(par.getAccount(), false);
            }
            sendPacket(authResponse);
        } else {
            forceClose(LoginServerFail.NOT_AUTHED);
        }
    }

    private void onReceiveServerStatus(byte[] data) {
        if (isAuthed()) {
            if (LoginServerConfig.DEBUG) {
                LOGGER.info("ServerStatus received");
            }
            new ServerStatus(data, getServerId()); // server status
        } else {
            forceClose(LoginServerFail.NOT_AUTHED);
        }
    }

    private void handleRegProcess(GameServerAuth gameServerAuth) {

        final int id = gameServerAuth.getDesiredID();
        final byte[] hexId = gameServerAuth.getHexID();

        GameServerInfo gsi = GameServerInstanceManager.getInstance().getServerInfo(id);
        // is there a gameserver registered with this id?
        if (gsi != null) {
            // does the hex id match?
            if (Arrays.equals(gsi.getHexId(), hexId)) {
                // check to see if this GS is already connected
                synchronized (gsi) {
                    if (gsi.isAuthed()) {
                        forceClose(LoginServerFail.REASON_ALREADY_LOGGED8IN);
                    } else {
                        attachGameServerInfo(gsi, gameServerAuth);
                    }
                }
            } else {
                // there is already a server registered with the desired id and different hex id
                // try to register this one with an alternative id
                if (LoginServerConfig.ACCEPT_NEW_GAMESERVER && gameServerAuth.acceptAlternateID()) {
                    gsi = new GameServerInfo(id, hexId, this);

                    if (GameServerInstanceManager.getInstance().registerWithFirstAvailableId(gsi)) {
                        attachGameServerInfo(gsi, gameServerAuth);
                        GameServerInstanceManager.getInstance().registerGameServer(gsi);
                    } else {
                        forceClose(LoginServerFail.REASON_NO_FREE_ID);
                    }
                } else {
                    // server id is already taken, and we cant get a new one for you
                    forceClose(LoginServerFail.REASON_WRONG_HEXID);
                }
            }
        } else {
            // can we register on this id?
            if (LoginServerConfig.ACCEPT_NEW_GAMESERVER) {
                gsi = new GameServerInfo(id, hexId, this);

                if (GameServerInstanceManager.getInstance().register(id, gsi)) {
                    attachGameServerInfo(gsi, gameServerAuth);
                    GameServerInstanceManager.getInstance().registerGameServer(gsi);
                } else {
                    // some one took this ID meanwhile
                    forceClose(LoginServerFail.REASON_ID_RESERVED);
                }
            } else {
                forceClose(LoginServerFail.REASON_WRONG_HEXID);
            }
        }
    }

    @Override
    public boolean hasAccountOnGameServer(String account) {
        return _accountsOnGameServer.contains(account);
    }

    @Override
    public int getPlayerCount() {
        return _accountsOnGameServer.size();
    }

    /**
     * Attachs a GameServerInfo to this Thread <li>Updates the GameServerInfo values based on GameServerAuth packet</li> <li><b>Sets the GameServerInfo as Authed</b></li>
     *
     * @param gsi            The GameServerInfo to be attached.
     * @param gameServerAuth The server info.
     */
    private void attachGameServerInfo(GameServerInfo gsi, GameServerAuth gameServerAuth) {
        setGameServerInfo(gsi);
        gsi.setGameServerThread(this);
        gsi.setPort(gameServerAuth.getPort());
        setGameHosts(gameServerAuth.getExternalHost(), gameServerAuth.getInternalHost());
        gsi.setMaxPlayers(gameServerAuth.getMaxPlayers());
        gsi.setAuthed(true);
    }

    private void forceClose(int reason) {
        final LoginServerFail lsf = new LoginServerFail(reason);

        try {
            sendPacket(lsf);
        } catch (final IOException e) {
            if (LoginServerConfig.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();

            LOGGER.error("GameServerThread: Failed kicking banned server. Reason: " + e.getMessage());
        }

        try {
            _connection.close();
        } catch (final IOException e) {
            if (LoginServerConfig.ENABLE_ALL_EXCEPTIONS)
                e.printStackTrace();

            LOGGER.error("GameServerThread: Failed disconnecting banned server, server already disconnected.");
        }
    }

    /**
     * @param sl
     * @throws java.io.IOException
     */
    private void sendPacket(ServerBasePacket sl) throws IOException {
        byte[] data = sl.getContent();
        NewCrypt.appendChecksum(data);

        if (LoginServerConfig.DEBUG) {
            LOGGER.debug("[S] " + sl.getClass().getSimpleName() + ":\n" + Util.printData(data));
        }
        data = _blowfish.crypt(data);

        final int len = data.length + 2;
        synchronized (_out) {
            _out.write(len & 0xff);
            _out.write(len >> 8 & 0xff);
            _out.write(data);
            _out.flush();
        }
    }

    @Override
    public void kickPlayer(String account) {
        final KickPlayer kp = new KickPlayer(account);
        try {
            sendPacket(kp);
        } catch (final IOException e) {
            LOGGER.error("", e);
        }
    }

    /**
     * @param gameExternalHost
     * @param gameInternalHost
     */
    public void setGameHosts(String gameExternalHost, String gameInternalHost) {
        final String oldInternal = _gsi.getInternalHost();
        final String oldExternal = _gsi.getExternalHost();

        _gsi.setExternalHost(gameExternalHost);
        _gsi.setInternalIp(gameInternalHost);

        if (!gameExternalHost.equals("*")) {
            try {
                _gsi.setExternalIp(InetAddress.getByName(gameExternalHost).getHostAddress());
            } catch (final UnknownHostException e) {
                LOGGER.error("Couldn't resolve hostname \"" + gameExternalHost + "\"", e);
            }
        } else {
            _gsi.setExternalIp(_connectionIp);
        }

        if (!gameInternalHost.equals("*")) {
            try {
                _gsi.setInternalIp(InetAddress.getByName(gameInternalHost).getHostAddress());
            } catch (final UnknownHostException e) {
                LOGGER.error("Couldn't resolve hostname \"" + gameInternalHost + "\"", e);
            }
        } else {
            _gsi.setInternalIp(_connectionIp);
        }

        LOGGER.info("Updated Gameserver [" + getServerId() + "] " + GameServerInstanceManager.getInstance().getServerName(getServerId()) + " IP's:");

        if (oldInternal == null || !oldInternal.equalsIgnoreCase(gameInternalHost)) {
            LOGGER.info("InternalIP: " + gameInternalHost);
        }

        if (oldExternal == null || !oldExternal.equalsIgnoreCase(gameExternalHost)) {
            LOGGER.info("ExternalIP: " + gameExternalHost);
        }
    }

    /**
     * @return Returns the isAuthed.
     */
    public boolean isAuthed() {
        return getGameServerInfo() != null && getGameServerInfo().isAuthed();

    }

    public GameServerInfo getGameServerInfo() {
        return _gsi;
    }

    public void setGameServerInfo(GameServerInfo gsi) {
        _gsi = gsi;
    }

    /**
     * @return Returns the connectionIpAddress.
     */
    public String getConnectionIpAddress() {
        return _connectionIPAddress;
    }

    private int getServerId() {
        if (getGameServerInfo() != null)
            return getGameServerInfo().getId();

        return -1;
    }
}
