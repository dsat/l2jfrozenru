package com.l2jfrozen.database.exception;

/**
 * Generic manager exception.
 */
public class GenericManagerException extends Exception
{
	/**
	 * Default constructor.
	 * 
	 * @param message
	 *            message
	 */
	public GenericManagerException(String message)
	{
		super(message);
	}

	/**
	 * Constructor with cause.
	 * 
	 * @param message
	 *            message
	 * @param cause
	 *            cause
	 */
	public GenericManagerException(String message, Throwable cause)
	{
		super(message, cause);
	}
}
