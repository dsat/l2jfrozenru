package com.l2jfrozen.database.dal.game.impl;

import com.l2jfrozen.database.dal.game.CharacterSkillManagerDAO;
import com.l2jfrozen.database.dal.impl.ManagerImpl;
import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.database.model.game.character.CharacterSkill;
import com.l2jfrozen.database.model.game.character.CharacterSkillsSave;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;

/**
 * author vadim.didenko 22.01.14
 */
@Repository
public class CharacterSkillManagerDAOImpl extends ManagerImpl implements CharacterSkillManagerDAO {

	@Override
	@Transactional
    public CharacterSkill addSkill(CharacterSkill skill) {
		getCurrentSession().persist(skill);
		return skill;
	}

	@Override
	@Transactional
	public CharacterSkill updateSkill(CharacterSkill skill)	{
		getCurrentSession().merge(skill);
		return skill;
	}

	@Override
	@Transactional
    public void deleteSkills(Long charId, int classIndex) {
		try {
			final CharacterEntity characterEntity = CharacterManager.getInstance().get(charId);
			final List<CharacterSkill> skillsToDelete = getSavedSkillsList(characterEntity, classIndex);
			for (final CharacterSkill current : skillsToDelete) {
				characterEntity.getSkills().remove(current.getId());
				getCurrentSession().delete(current);
			}
		} catch (final HibernateException e) {
			LOGGER.error(String.format("Cannot deleteEntity entity! %s",e.toString()));
		}
	}

	@Override
	@Transactional
	public CharacterEntity getSavedSkills(CharacterEntity entity, int classIndex)	{
		List<CharacterSkillsSave> skillsSaves;
		entity.setSkillsSave(new HashSet<CharacterSkillsSave>());
        skillsSaves = getCurrentSession().createCriteria(CharacterSkillsSave.class).add(Restrictions.and(Restrictions.eq("classIndex", classIndex),Restrictions.eq("character", entity))).list();
		for (final CharacterSkillsSave skillsSave : skillsSaves) {
			entity.getSkillsSave().add(skillsSave);
		}
		return entity;
	}

	@Override
	public List<CharacterSkill> getSavedSkillsList(CharacterEntity entity,
			int classIndex) {
		final List<CharacterSkill> skillsSaves = getCurrentSession().createCriteria(CharacterSkill.class).add(Restrictions.and(Restrictions.eq("classIndex", classIndex),Restrictions.eq("character", entity))).list();

		return skillsSaves;
	}
}
