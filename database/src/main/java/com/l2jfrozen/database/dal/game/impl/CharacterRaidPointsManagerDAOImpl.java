package com.l2jfrozen.database.dal.game.impl;

import com.l2jfrozen.database.dal.game.CharacterRaidPointsManagerDAO;
import com.l2jfrozen.database.dal.impl.ManagerImpl;
import com.l2jfrozen.database.model.game.character.CharacterRaidPoints;
import org.hibernate.HibernateException;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * author vadim.didenko 1/11/14.
 */
@Repository
public class CharacterRaidPointsManagerDAOImpl extends ManagerImpl implements CharacterRaidPointsManagerDAO
{
    @Override
    @Transactional
    @Cacheable("gameCache")
    public CharacterRaidPoints create(CharacterRaidPoints raidPoints) {
        try
        {
            raidPoints.setId(null);
            getCurrentSession().persist(raidPoints);
            return raidPoints;
        }
        catch (final Exception ex)
        {
            LOGGER.error("Got hibernate exception.", ex);
        }
        return null;
    }

    @Override
    @Transactional
    @Cacheable("gameCache")
    public void delete(CharacterRaidPoints raidPoints) {

        try
        {
            getCurrentSession().delete(raidPoints);
            getCurrentSession().flush();
            getCurrentSession().clear();
        }
        catch (final HibernateException e)
        {
            LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
        }

    }

    @Override
    @Transactional
    @Cacheable("gameCache")
    public void delete(long id) {

        try
        {
            final CharacterRaidPoints raidPoints = (CharacterRaidPoints) getCurrentSession().createCriteria(CharacterRaidPoints.class).add(Restrictions.eq("id", id)).uniqueResult();
            getCurrentSession().delete(raidPoints);
        }
        catch (final HibernateException e)
        {
            LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
        }

    }

    @Override
    @Transactional
    @Cacheable("gameCache")
    public void delete(Long ownerId, List<Long> bossIds) {

        try
        {
            getCurrentSession().createQuery("DELETE FROM CharacterRaidPoints WHERE character.id=:charId AND" + buildConstraintOR("bossId", bossIds)).setLong("charId", ownerId);
        }
        catch (final HibernateException e)
        {
            LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
        }

    }

    @Override
    @Transactional
    @Cacheable("gameCache")
    public void cleanUpPoints() {

        try
        {
       			// TODO: Improve system
			getCurrentSession().createQuery("DELETE FROM CharacterRaidPoints WHERE charId>0");
        }
        catch (final HibernateException e)
        {
            LOGGER.error(String.format("Cannot deleteEntity entity! %s", e.toString()));
        }

    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public CharacterRaidPoints get(long _Id) {

        try
        {
            final CharacterRaidPoints raidPoints = (CharacterRaidPoints) getCurrentSession().createCriteria(CharacterRaidPoints.class).add(Restrictions.eq("id", _Id)).uniqueResult();
            return raidPoints;
        }
        catch (final Exception e)
        {
            LOGGER.error("Got hibernate exception.", e);
        }
        return null;

    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public CharacterRaidPoints get(int charId, int bossId){

        try
        {
            final CharacterRaidPoints raidPoints = (CharacterRaidPoints) getCurrentSession().createCriteria(CharacterRaidPoints.class).add(Restrictions.and(Restrictions.eq("charId", charId),Restrictions.eq("bossId", bossId))).uniqueResult();
            return raidPoints;
        }
        catch (final Exception e)
        {
            LOGGER.error("Got hibernate exception.", e);
        }
        return null;

    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public List<CharacterRaidPoints> getCharRaidPoints(int ownerId) {
        try
        {
            return getCurrentSession().createCriteria(CharacterRaidPoints.class).add(Restrictions.eq("charId", ownerId)).list();
        }
        catch (final Exception e)
        {
            LOGGER.error("Got hibernate exception.", e);
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional(readOnly = true)
    @Cacheable("gameCache")
    public List<CharacterRaidPoints> getAllCharsRaidPoints() {
        try
        {
            return getCurrentSession().createCriteria(CharacterRaidPoints.class).list();
        }
        catch (final Exception e)
        {
            LOGGER.error("Got hibernate exception.", e);
        }
        return new ArrayList<>();
    }

    @Override
    @Transactional
    @Cacheable("gameCache")
    public CharacterRaidPoints update(CharacterRaidPoints raidPoints) {

        try
        {
            if (raidPoints.getId() == null)
            {
                return create(raidPoints);
            }
            getCurrentSession().update(raidPoints);
            return raidPoints;
        }
        catch (final HibernateException e)
        {
            LOGGER.error("Got hibernate exception.", e);
        }
        return null;

    }

}
