package com.l2jfrozen.gameserver.powerpak;

import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.gameserver.PlayerManager;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2ItemInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * L2JFrozen
 */
public class L2Utils {
    private static final Logger LOGGER = LoggerFactory.getLogger(L2Utils.class.getName());

    public static interface IItemFilter {
        public boolean isCanShow(L2ItemInstance item);
    }

    public static L2PcInstance loadPlayer(String charName) {
        L2PcInstance result = L2World.getInstance().getPlayer(charName);
        if (result == null) {
            try {
                final CharacterEntity character = CharacterManager.getInstance().getByCharacterName(charName);
                if (character == null) {
                    return null;
                }
                result = PlayerManager.getInstance().load(character.getId().intValue());
            } catch (final Exception e) {
                LOGGER.error("", e);
                result = null;
            }
        }

        return result;
    }

    public static String formatUserItems(L2PcInstance player, int startItem, IItemFilter filter, String actionString) {
        String result = "<table width=300>";
        int startwith = 0;
        for (final L2ItemInstance it : player.getInventory().getItems()) {
            if (startwith++ < startItem) continue;
            if (filter != null && !filter.isCanShow(it)) continue;
            result += "<tr><td>";
            if (actionString != null) {
                String s = actionString.replace("%itemid%", String.valueOf(it.getItemId()));
                s = s.replace("%objectId%", String.valueOf(it.getObjectId()));
                result += ("<a action=\"" + s + "\">");
            }

            if (it.getEnchantLevel() > 0) result += "+" + it.getEnchantLevel() + " ";
            result += it.getItemName();
            if (actionString != null)
                result += "</a>";
            result += "</td><td>";
            if (it.getCount() > 1) result += (it.getCount() + " pc.");
            result += "</td></tr>";
        }
        result += "<table>";
        return result;
    }

    public static String loadMessage(String msg) {
        if (msg.startsWith("@")) {
            msg = msg.substring(1);
            final int iPos = msg.indexOf(";");
            if (iPos != -1) {
                final StringTable st = new StringTable(msg.substring(0, iPos));
                return st.Message(msg.substring(iPos + 1));
            }
        }
        return msg;
    }

}
