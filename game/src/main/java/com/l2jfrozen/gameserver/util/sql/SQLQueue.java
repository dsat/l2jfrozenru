/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.util.sql;

import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import com.l2jfrozen.util.CloseUtil;
import com.l2jfrozen.util.database.L2DatabaseFactory;
import javolution.util.FastList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.ScheduledFuture;

/**
 * @author L2JFrozen
 */
public class SQLQueue implements Runnable {
    private static final Logger LOGGER = LoggerFactory.getLogger(SQLQueue.class.getName());
    private static SQLQueue _instance = null;
    private final FastList<SQLQuery> _query;
    private final ScheduledFuture<?> _task;
    private boolean _inShutdown;
    private boolean _isRuning;

    private SQLQueue() {
        _query = new FastList<SQLQuery>();
        _task = ThreadPoolManager.getInstance().scheduleGeneralAtFixedRate(this, 60000, 60000);

    }

    public static SQLQueue getInstance() {
        if (_instance == null)
            _instance = new SQLQueue();
        return _instance;
    }

    public void shutdown() {
        _inShutdown = true;
        _task.cancel(false);
        if (!_isRuning && _query.size() > 0)
            run();

    }

    public void add(SQLQuery q) {
        if (!_inShutdown)
            _query.addLast(q);
    }

    @Override
    public void run() {
        _isRuning = true;
        synchronized (_query) {
            while (_query.size() > 0) {
                final SQLQuery q = _query.removeFirst();
                Connection _con = null;
                try {
                    _con = L2DatabaseFactory.getInstance().getConnection(false);

                    q.execute(_con);
                } catch (final SQLException e) {
                    LOGGER.error("", e);
                } finally {

                    CloseUtil.close(_con);
                    _con = null;
                }
            }
        }
        /*TODO Need investigate this logic
        Connection _con = null;
        try {
            _con = L2DatabaseFactory.getInstance().getConnection(false);
            PreparedStatement stm = _con.prepareStatement("select * from characters where char_name is null");
            stm.execute();
            stm.close();

        } catch (SQLException e) {
            LOGGER.error("", e);
        } finally {
            CloseUtil.close(_con);
            _con = null;
        }*/
        _isRuning = false;
    }
}
