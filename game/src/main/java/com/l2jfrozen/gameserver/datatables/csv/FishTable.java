/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.datatables.csv;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.model.FishData;
import javolution.util.FastList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;
import java.util.StringTokenizer;

/**
 * @author -Nemesiss-
 */
public class FishTable {
    private static FishTable _instance;
    public static FishData fish;
    private static Logger LOGGER = LoggerFactory.getLogger(FishTable.class.getName());
    private static List<FishData> _fishsNormal;
    private static List<FishData> _fishsEasy;
    private static List<FishData> _fishsHard;

    private FishTable() {
        int count = 0;

        FileReader reader = null;
        BufferedReader buff = null;
        LineNumberReader lnr = null;

        try {
            final File fileData = new File(GameServerConfig.DATAPACK_ROOT + "/data/csv/fish.csv");

            reader = new FileReader(fileData);
            buff = new BufferedReader(reader);
            lnr = new LineNumberReader(buff);

            String line = null;

            _fishsEasy = new FastList<FishData>();
            _fishsNormal = new FastList<FishData>();
            _fishsHard = new FastList<FishData>();
            FishData fish;

            //format:
            // id;level;name;hp;hpregen;fish_type;fish_group;fish_guts;guts_check_time;wait_time;combat_time
            while ((line = lnr.readLine()) != null) {
                //ignore comments
                if (line.trim().length() == 0 || line.startsWith("#")) {
                    continue;
                }

                final StringTokenizer st = new StringTokenizer(line, ";");

                final int id = Integer.parseInt(st.nextToken());
                final int lvl = Integer.parseInt(st.nextToken());
                final String name = st.nextToken();
                final int hp = Integer.parseInt(st.nextToken());
                final int hpreg = Integer.parseInt(st.nextToken());
                final int type = Integer.parseInt(st.nextToken());
                final int group = Integer.parseInt(st.nextToken());
                final int fish_guts = Integer.parseInt(st.nextToken());
                final int guts_check_time = Integer.parseInt(st.nextToken());
                final int wait_time = Integer.parseInt(st.nextToken());
                final int combat_time = Integer.parseInt(st.nextToken());

                fish = new FishData(id, lvl, name, hp, hpreg, type, group, fish_guts, guts_check_time, wait_time, combat_time);
                switch (fish.getGroup()) {
                    case 0:
                        _fishsEasy.add(fish);
                        break;
                    case 1:
                        _fishsNormal.add(fish);
                        break;
                    case 2:
                        _fishsHard.add(fish);
                }
            }

            count = _fishsEasy.size() + _fishsNormal.size() + _fishsHard.size();

        } catch (final FileNotFoundException e) {


            LOGGER.error("fish.csv is missing in data folder", e);
        } catch (final IOException e0) {

            LOGGER.error("Error while creating table: ", e0);
        } finally {
            if (lnr != null)
                try {
                    lnr.close();
                } catch (final Exception e1) {
                    LOGGER.error("", e1);
                }

            if (buff != null)
                try {
                    buff.close();
                } catch (final Exception e1) {
                    LOGGER.error("", e1);
                }

            if (reader != null)
                try {
                    reader.close();
                } catch (final Exception e1) {
                    LOGGER.error("", e1);
                }
        }
        LOGGER.info("FishTable: Loaded " + count + " Fishes.");

    }

    public static FishTable getInstance() {
        if (_instance == null) {
            _instance = new FishTable();
        }
        return _instance;
    }

    /**
     * @param lvl
     * @param type
     * @param group
     * @return List of Fish that can be fished
     */
    public List<FishData> getfish(int lvl, int type, int group) {
        final List<FishData> result = new FastList<FishData>();
        List<FishData> _Fishs = null;

        switch (group) {
            case 0:
                _Fishs = _fishsEasy;
                break;
            case 1:
                _Fishs = _fishsNormal;
                break;
            case 2:
                _Fishs = _fishsHard;
        }
        if (_Fishs == null) {
            // the fish list is empty
            LOGGER.warn("Fish are not defined !");
            return null;
        }
        for (final FishData f : _Fishs) {
            if (f.getLevel() != lvl) {
                continue;
            }

            if (f.getType() != type) {
                continue;
            }

            result.add(f);
        }
        if (result.size() == 0) {
            LOGGER.warn("Cant Find Any Fish!? - Lvl: " + lvl + " Type: " + type);
        }

        _Fishs = null;

        return result;
    }

}
