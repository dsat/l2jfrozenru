/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.datatables.sql;

import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class ...
 *
 * @version $Revision: 1.3.2.2.2.1 $ $Date: 2005/03/27 15:29:18 $
 */
public class CharNameTable {
    private final static Logger LOGGER = LoggerFactory.getLogger(CharNameTable.class.getName());

    private static CharNameTable _instance;

    public static CharNameTable getInstance() {
        if (_instance == null) {
            _instance = new CharNameTable();
        }
        return _instance;
    }

    public synchronized boolean doesCharNameExist(String name) {
        final CharacterEntity character = CharacterManager.getInstance().getByCharacterName(name);
        return character != null;
    }

    public int accountCharNumber(String account) {

        return CharacterManager.getInstance().get(account).size();
    }
}
