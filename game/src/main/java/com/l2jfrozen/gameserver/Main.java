package com.l2jfrozen.gameserver;

import com.l2jfrozen.ServerTitle;
import com.l2jfrozen.context.ApplicationContextManager;
import com.l2jfrozen.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * vadim.didenko
 * 2/22/14.
 */
public class Main {
    private static final String SPRING_CONFIG_LOCATION = "classpath:config/bean/spring-config-game.xml";
    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class.getName());

    public static void main(String[] args) {
		/* TEAM LOGO */
		Util.printSection("Team");
		ServerTitle.info();

		Util.printSection("Spring");
        LOGGER.info("Start server");
        LOGGER.info("Spring initialize");
        ApplicationContextManager.init(SPRING_CONFIG_LOCATION);
		LOGGER.info("Config server initializing...");
        ApplicationContextManager.getApplicationContext();
    }
}
