/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.network.serverpackets;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.model.TradeList.TradeItem;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;

/**
 * This class ...
 *
 * @version $Revision: 1.2.2.3.2.6 $ $Date: 2005/03/27 15:29:57 $
 */
public class PrivateStoreListSell extends L2GameServerPacket {
    // private static final String _S__B4_PRIVATEBUYLISTSELL = "[S] 9b PrivateBuyListSell";
    private static final String _S__B4_PRIVATESTORELISTSELL = "[S] 9b PrivateStoreListSell";
    private int _objId;
    private int _playerAdena;
    private final boolean _packageSale;
    private TradeItem[] _items;

    // player's private shop
    public PrivateStoreListSell(L2PcInstance player, L2PcInstance storePlayer) {
       if (GameServerConfig.SELL_BY_ITEM) {
            final CreatureSay cs11 = new CreatureSay(0, 15, "", "ATTENTION: Store System is not based on Adena, be careful!"); // 8D
           player.sendPacket(cs11);
           _playerAdena = player.getItemCount(GameServerConfig.SELL_ITEM, -1);
        } else  {
                       _playerAdena = player.getAdena();
        }

        _objId = storePlayer.getObjectId();
        storePlayer.getSellList().updateItems();
        _items = storePlayer.getSellList().getItems();
         _packageSale = storePlayer.getSellList().isPackaged();
    }

    @Override
    protected final void writeImpl() {
        writeC(0x9b);
        writeD(_objId);
        writeD(_packageSale ? 1 : 0);
        writeD(_playerAdena);

        writeD(_items.length);
        for (TradeItem item : _items) {
            writeD(item.getItem().getType2());
            writeD(item.getObjectId());
            writeD(item.getItem().getItemId());
            writeD(item.getCount());
            writeH(0x00);
            writeH(item.getEnchant());
            writeH(0x00);
            writeD(item.getItem().getBodyPart());
            writeD(item.getPrice()); // your price
            writeD(item.getItem().getReferencePrice()); // store price
        }
    }

    @Override
    public String getType() {
        return _S__B4_PRIVATESTORELISTSELL;
    }
}