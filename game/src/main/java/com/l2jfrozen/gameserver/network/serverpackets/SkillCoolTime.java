package com.l2jfrozen.gameserver.network.serverpackets;

import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;

import java.util.Collection;
import java.util.Iterator;

public class SkillCoolTime extends L2GameServerPacket {

    @SuppressWarnings("rawtypes")
    public Collection _reuseTimeStamps;

    public SkillCoolTime(L2PcInstance cha) {
        _reuseTimeStamps = cha.getReuseTimeStamps();
    }
    @SuppressWarnings("rawtypes")
    @Override
    protected final void writeImpl() {
        @SuppressWarnings("cast")
		final
        L2PcInstance activeChar = getClient().getActiveChar();
        if (activeChar == null)
            return;
        writeC(0xc7);
        writeD(_reuseTimeStamps.size());
        L2PcInstance.TimeStamp ts;
        for (final Iterator i$ = _reuseTimeStamps.iterator(); i$.hasNext(); writeD((int) ts.getRemaining() / 1000)) {
            ts = (L2PcInstance.TimeStamp) i$.next();
			writeD(ts.getSkill().getId()); // Skill Id
			writeD(ts.getSkill().getLevel()); // Skill Level
			writeD(0); // Total reuse delay, seconds
			writeD((int) ts.getReuse() / 1000); // Time remaining, seconds
        }
    }

    @Override
    public String getType() {
        return "[S] c1 SkillCoolTime";
    }
}