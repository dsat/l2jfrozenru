/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.admincommandhandlers;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.handler.IAdminCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.gameserverpackets.GameServerStatus;
import com.l2jfrozen.gameserver.network.serverpackets.NpcHtmlMessage;
import com.l2jfrozen.gameserver.thread.LoginServerThread;
import com.l2jfrozen.thread.ServerStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.StringTokenizer;

/**
 * This class handles the admin commands that acts on the login
 *
 * @version $Revision: 1.2.2.1.2.4 $ $Date: 2007/07/31 10:05:56 $
 */
public class AdminLogin implements IAdminCommandHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminLogin.class.getName());

    private static final String[] ADMIN_COMMANDS =
            {
                    "admin_server_gm_only", "admin_server_all", "admin_server_max_player", "admin_server_list_clock", "admin_serverLOGGERin"
            };


    @Override
    public boolean useAdminCommand(String command, L2PcInstance activeChar) {


        if (command.equals("admin_server_gm_only")) {
            gmOnly();
            activeChar.sendMessage("Server is now GM only");
            showMainPage(activeChar);
        } else if (command.equals("admin_server_all")) {
            allowToAll();
            activeChar.sendMessage("Server is not GM only anymore");
            showMainPage(activeChar);
        } else if (command.startsWith("admin_server_max_player")) {
            StringTokenizer st = new StringTokenizer(command);
            if (st.countTokens() > 1) {
                st.nextToken();
                String number = st.nextToken();
                try {
                    LoginServerThread.getInstance().setMaxPlayer(Integer.parseInt(number));
                    activeChar.sendMessage("maxPlayer set to " + Integer.parseInt(number));
                    showMainPage(activeChar);
                } catch (final NumberFormatException e) {
                    LOGGER.error("", e);

                    activeChar.sendMessage("Max players must be a number.");
                }

                number = null;
            } else {
                activeChar.sendMessage("Format is server_max_player <max>");
            }

            st = null;
        } else if (command.startsWith("admin_server_list_clock")) {
            StringTokenizer st = new StringTokenizer(command);

            if (st.countTokens() > 1) {
                st.nextToken();
                String mode = st.nextToken();

                if (mode.equals("on")) {
                    LoginServerThread.getInstance().sendServerStatus(GameServerStatus.SERVER_LIST_CLOCK, GameServerStatus.ON);
                    activeChar.sendMessage("A clock will now be displayed next to the server name");
                    GameServerConfig.SERVER_LIST_CLOCK = true;
                    showMainPage(activeChar);
                } else if (mode.equals("off")) {
                    LoginServerThread.getInstance().sendServerStatus(GameServerStatus.SERVER_LIST_CLOCK, GameServerStatus.OFF);
                    GameServerConfig.SERVER_LIST_CLOCK = false;
                    activeChar.sendMessage("The clock will not be displayed");
                    showMainPage(activeChar);
                } else {
                    activeChar.sendMessage("Format is server_list_clock <on/off>");
                }

                mode = null;
            } else {
                activeChar.sendMessage("Format is server_list_clock <on/off>");
            }

            st = null;
        } else if (command.equals("admin_serverLOGGERin")) {
            showMainPage(activeChar);
        }
        return true;
    }

    /**
     * @param activeChar
     */
    private void showMainPage(L2PcInstance activeChar) {
        NpcHtmlMessage html = new NpcHtmlMessage(1);
        html.setFile("data/html/admin/login.htm");
        html.replace("%server_name%", LoginServerThread.getInstance().getServerName());
        html.replace("%status%", LoginServerThread.getInstance().getStatusString());
        html.replace("%clock%", String.valueOf(GameServerConfig.SERVER_LIST_CLOCK));
        html.replace("%brackets%", String.valueOf(GameServerConfig.SERVER_LIST_BRACKET));
        html.replace("%max_players%", String.valueOf(LoginServerThread.getInstance().getMaxPlayer()));
        activeChar.sendPacket(html);

        html = null;
    }

    private void allowToAll() {
        LoginServerThread.getInstance().setServerStatus(ServerStatus.STATUS_AUTO);
        GameServerConfig.SERVER_GMONLY = false;
    }

    private void gmOnly() {
        LoginServerThread.getInstance().setServerStatus(ServerStatus.STATUS_GM_ONLY);
        GameServerConfig.SERVER_GMONLY = true;
    }

    /* (non-Javadoc)
     * @see com.l2jfrozen.gameserver.handler.IAdminCommandHandler#getAdminCommandList()
     */
    @Override
    public String[] getAdminCommandList() {
        return ADMIN_COMMANDS;
    }

}
