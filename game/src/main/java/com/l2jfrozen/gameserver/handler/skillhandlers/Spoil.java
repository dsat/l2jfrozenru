/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.skillhandlers;

import com.l2jfrozen.gameserver.ai.CtrlEvent;
import com.l2jfrozen.gameserver.handler.ISkillHandler;
import com.l2jfrozen.gameserver.model.L2Character;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.model.actor.instance.L2MonsterInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.SystemMessageId;
import com.l2jfrozen.gameserver.network.serverpackets.SystemMessage;
import com.l2jfrozen.gameserver.skills.Formulas;
import com.l2jfrozen.util.GArray;

public class Spoil implements ISkillHandler<L2Character> {
    private static final SkillType[] SKILL_IDS = {SkillType.SPOIL};

    @Override
    public <A extends L2Character> void useSkill(A activeChar, L2Skill skill, GArray<L2Character> targets) {
        if (!(activeChar instanceof L2PcInstance)) {
            return;
        }

        for (final L2Character target : targets) {
            final L2MonsterInstance monsterInstance = (L2MonsterInstance) target;
            if (monsterInstance.isSpoil()) {
                activeChar.sendPacket(new SystemMessage(SystemMessageId.ALREDAY_SPOILED));
                continue;
            }
            boolean spoil;
            if (!monsterInstance.isDead()) {
                spoil = Formulas.calcMagicSuccess(activeChar, monsterInstance, skill);

                if (spoil) {
                    monsterInstance.setSpoil(true);
                    monsterInstance.setIsSpoiledBy(activeChar.getObjectId());
                    activeChar.sendPacket(new SystemMessage(SystemMessageId.SPOIL_SUCCESS));
                } else {
                    final SystemMessage sm = new SystemMessage(SystemMessageId.S1_WAS_UNAFFECTED_BY_S2);
                    sm.addString(monsterInstance.getName());
                    sm.addSkillName(skill.getDisplayId());
                    activeChar.sendPacket(sm);
                }
                monsterInstance.getAI().notifyEvent(CtrlEvent.EVT_ATTACKED, activeChar);
            }
        }
    }

    @Override
    public SkillType[] getSkillIds() {
        return SKILL_IDS;
    }
}
