package com.l2jfrozen.gameserver.handler.voicedcommandhandlers;

import com.l2jfrozen.gameserver.handler.IVoicedCommandHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;

public class ExpGain implements IVoicedCommandHandler {
    private String[] _voicedCommands = {
            "expon",
            "xpon",
            "expoff",
            "xpoff"
    };

    public boolean useVoicedCommand(String command, L2PcInstance activeChar, String target) {
        if (command.equalsIgnoreCase("expon") || command.equalsIgnoreCase("xpon")) {
            activeChar.setExpOn(true);
            activeChar.sendMessage("EXP ON Enabled.");
        }
        else if (command.equalsIgnoreCase("expoff") || command.equalsIgnoreCase("xpoff")) {
            activeChar.setExpOn(false);
            activeChar.sendMessage("EXP OFF Disabled.");
        }
        return true;
    }

    public String[] getVoicedCommandList() {
        return _voicedCommands;
    }
}
	