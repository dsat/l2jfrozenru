// Donator Custom Item , Created By Stefoulis15
// Added From Stefoulis15 Into The Core.
// Visit www.MaxCheaters.com For Support 
// Source File Name:   HeroCustomItem.java
// New File Name: 	DonatorCustomItem.java
// Modded by programmos, sword dev
// Adapted for Donator items by Karech

package com.l2jfrozen.gameserver.handler.itemhandlers;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.database.manager.CommonManager;
import com.l2jfrozen.database.model.game.character.CharacterCustomData;
import com.l2jfrozen.gameserver.handler.IItemHandler;
import com.l2jfrozen.gameserver.model.actor.instance.L2ItemInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.model.actor.instance.L2PlayableInstance;
import com.l2jfrozen.gameserver.network.serverpackets.SocialAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DonatorCustomItem implements IItemHandler
{

	protected static final Logger LOGGER = LoggerFactory.getLogger(HeroCustomItem.class.getName());

	@Override
	public void useItem(L2PlayableInstance playable, L2ItemInstance item)
	{
		if(GameServerConfig.DONATOR_CUSTOM_ITEMS)
		{
			if(!(playable instanceof L2PcInstance))
			{
				return;
			}

			L2PcInstance activeChar = (L2PcInstance) playable;

			if(activeChar.isDonator()){
				activeChar.sendMessage("You Are Already A Donator!.");
			}else{
				activeChar.broadcastPacket(new SocialAction(activeChar.getObjectId(), 16));
				activeChar.setDonator(true);
                activeChar.broadcastUserInfo();
				updateDatabase(activeChar, GameServerConfig.DONATOR_CUSTOM_DAY * 24L * 60L * 60L * 1000L);
				activeChar.updateNameTitleColor();
				
				if (!activeChar.isDonator()) 
				{
					activeChar.sendMessage("You Are Now a Donator! Your rates are now increased!");
				} 
				else
				{
					activeChar.sendMessage("Your Donator time has been extended for " + GameServerConfig.DONATOR_CUSTOM_DAY + " day(s).");
				}	
				
				activeChar.broadcastUserInfo();
				playable.destroyItem("Consume", item.getObjectId(), 1, null, false);
			}
			activeChar = null;
		}
	}

	@Override
	public int[] getItemIds()
	{
		return ITEM_IDS;
	}

    private void updateDatabase(L2PcInstance player, long donatorTime) {
        try {
            if (player == null) {
                return;
            }
            CharacterCustomData characterCustomData = player.getCharacter().getCustomData();
            if (characterCustomData == null) {
                characterCustomData = new CharacterCustomData();
            }

            characterCustomData.setCharacter(player.getCharacter());
            characterCustomData.setCharName(player.getName());
            characterCustomData.setDonator(1);
            characterCustomData.setDonatorEndDate(donatorTime == 0 ? 0 : System.currentTimeMillis() + donatorTime);
            if (characterCustomData.getId() == null) {
                CommonManager.getInstance().saveNew(characterCustomData);
                player.getCharacter().setCustomData(characterCustomData);
            } else {
                CommonManager.getInstance().update(characterCustomData);
            }
        } catch (Exception e) {
            LOGGER.error("", e);

            LOGGER.info("Error: could not update database: ", e);
        }
    }
	private static final int ITEM_IDS[] =
	{
            GameServerConfig.DONATOR_CUSTOM_ITEM_ID
	};

}