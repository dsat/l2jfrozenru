/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.handler.admincommandhandlers;

import com.l2jfrozen.database.manager.game.CharacterManager;
import com.l2jfrozen.database.model.game.character.CharacterEntity;
import com.l2jfrozen.gameserver.handler.IAdminCommandHandler;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class handles following admin commands: - delete = deletes target
 *
 * @version $Revision: 1.1.2.6.2.3 $ $Date: 2005/04/11 10:05:59 $
 */

public class AdminRepairChar implements IAdminCommandHandler {
    private static Logger LOGGER = LoggerFactory.getLogger(AdminRepairChar.class.getName());

    private static final String[] ADMIN_COMMANDS =
            {
                    "admin_restore", "admin_repair"
            };

    @Override
    public boolean useAdminCommand(String command, L2PcInstance activeChar) {
        handleRepair(command);
        return true;
    }

    @Override
    public String[] getAdminCommandList() {
        return ADMIN_COMMANDS;
    }

    private void handleRepair(String command) {
        final String[] parts = command.split(" ");

        if (parts.length != 2) {
            return;
        }
        final CharacterEntity character = L2World.getInstance().getPlayer(parts[1]).getCharacter();
        if (character == null) {
            return;
        }
        character.setX(-84318);
        character.setY(244579);
        character.setZ(3730);
        CharacterManager.getInstance().deleteShortcuts(character);
        character.getShortcuts().clear();

        CharacterManager.getInstance().update(character);
    }
}
