/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.managers;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.ai.CtrlIntention;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.network.serverpackets.SetupGauge;
import com.l2jfrozen.gameserver.network.serverpackets.SocialAction;
import com.l2jfrozen.gameserver.thread.ThreadPoolManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;

/**
 * @author Michiru
 */
public final class AwayManager {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AwayManager.class.getName());
    private static AwayManager _instance;
    protected Map<L2PcInstance, RestoreData> awayPlayers;

    public static final AwayManager getInstance() {
        if (_instance == null) {
            _instance = new AwayManager();
            LOGGER.info("AwayManager: initialized.");
        }
        return _instance;
    }

    private final class RestoreData {
        private final String originalTitle;
        private final int originalTitleColor;
        private final boolean sitForced;

        public RestoreData(L2PcInstance activeChar) {
            originalTitle = activeChar.getTitle();
            originalTitleColor = activeChar.getAppearance().getTitleColor();
            sitForced = !activeChar.isSitting();
        }

        public boolean isSitForced() {
            return sitForced;
        }

        public void restore(L2PcInstance activeChar) {
            activeChar.getAppearance().setTitleColor(originalTitleColor);
            activeChar.setTitle(originalTitle);
        }
    }

    private AwayManager() {
        awayPlayers = Collections.synchronizedMap(new WeakHashMap<L2PcInstance, RestoreData>());
    }

    /**
     * @param activeChar
     * @param text
     */
    public void setAway(L2PcInstance activeChar, String text) {
        activeChar.set_awaying(true);
        activeChar.broadcastPacket(new SocialAction(activeChar.getObjectId(), 9));
        activeChar.sendMessage("Your status is Away in " + GameServerConfig.AWAY_TIMER + " Sec.");
        activeChar.getAI().setIntention(CtrlIntention.AI_INTENTION_IDLE);
        SetupGauge sg = new SetupGauge(SetupGauge.BLUE, GameServerConfig.AWAY_TIMER * 1000);
        activeChar.sendPacket(sg);
        sg = null;
        activeChar.setIsImobilised(true);
        ThreadPoolManager.getInstance().scheduleGeneral(new PlayerAwayTask(activeChar, text), GameServerConfig.AWAY_TIMER * 1000);
    }

    /**
     * @param activeChar
     */
    public void setBack(L2PcInstance activeChar) {
        activeChar.sendMessage("You are back from Away Status in " + GameServerConfig.BACK_TIMER + " Sec.");
        SetupGauge sg = new SetupGauge(SetupGauge.BLUE, GameServerConfig.BACK_TIMER * 1000);
        activeChar.sendPacket(sg);
        sg = null;
        ThreadPoolManager.getInstance().scheduleGeneral(new PlayerBackTask(activeChar), GameServerConfig.BACK_TIMER * 1000);
    }

    public void extraBack(L2PcInstance activeChar) {
        if (activeChar == null) {
            return;
        }
        RestoreData rd = awayPlayers.get(activeChar);
        if (rd == null) {
            return;
        }

        rd.restore(activeChar);
        rd = null;
        awayPlayers.remove(activeChar);
    }

    class PlayerAwayTask implements Runnable {

        private final L2PcInstance activeChar;
        private final String awayText;

        PlayerAwayTask(L2PcInstance activeChar, String awayText) {
            this.activeChar = activeChar;
            this.awayText = awayText;
        }

        @Override
        public void run() {
            if (activeChar == null) {
                return;
            }
            if (activeChar.isAttackingNow() || activeChar.isCastingNow()) {
                return;
            }

            awayPlayers.put(activeChar, new RestoreData(activeChar));

            activeChar.disableAllSkills();
            activeChar.abortAttack();
            activeChar.abortCast();
            activeChar.setTarget(null);
            activeChar.setIsImobilised(false);
            if (!activeChar.isSitting()) {
                activeChar.sitDown();
            }
            if (awayText.length() <= 1) {
                activeChar.sendMessage("You are now *Away*");
            } else {
                activeChar.sendMessage("You are now Away *" + awayText + "*");
            }

            activeChar.getAppearance().setTitleColor(GameServerConfig.AWAY_TITLE_COLOR);

            if (awayText.length() <= 1) {
                activeChar.setTitle("*Away*");
            } else {
                activeChar.setTitle("Away*" + awayText + "*");
            }

            activeChar.broadcastUserInfo();
            activeChar.setIsParalyzed(true);
            activeChar.setIsAway(true);
            activeChar.set_awaying(false);
        }
    }

    class PlayerBackTask implements Runnable {

        private final L2PcInstance activeChar;

        PlayerBackTask(L2PcInstance activeChar) {
            this.activeChar = activeChar;
        }

        @Override
        public void run() {
            if (activeChar == null) {
                return;
            }
            RestoreData rd = awayPlayers.get(activeChar);

            if (rd == null) {
                return;
            }

            activeChar.setIsParalyzed(false);
            activeChar.enableAllSkills();
            activeChar.setIsAway(false);

            if (rd.isSitForced()) {
                activeChar.standUp();
            }

            rd.restore(activeChar);
            rd = null;
            awayPlayers.remove(activeChar);
            activeChar.broadcastUserInfo();
            activeChar.sendMessage("You are Back now!");
        }
    }
}
