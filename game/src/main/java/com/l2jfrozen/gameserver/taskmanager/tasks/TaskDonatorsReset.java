/* This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.taskmanager.tasks;

import java.util.Calendar;
import java.util.Map;
import java.sql.Connection;
import com.l2jfrozen.util.database.L2DatabaseFactory;
import java.sql.SQLException;
import java.sql.Statement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.datatables.sql.ClanTable;
import com.l2jfrozen.database.model.game.character.CharacterCustomData;
import com.l2jfrozen.gameserver.managers.RaidBossPointsManager;
import com.l2jfrozen.gameserver.model.L2Clan;
import com.l2jfrozen.gameserver.model.L2Object;
import com.l2jfrozen.gameserver.model.L2World;
import com.l2jfrozen.gameserver.model.actor.instance.L2PcInstance;
import com.l2jfrozen.gameserver.taskmanager.Task;
import com.l2jfrozen.gameserver.taskmanager.TaskManager;
import com.l2jfrozen.gameserver.taskmanager.TaskTypes;
import com.l2jfrozen.gameserver.taskmanager.TaskManager.ExecutedTask;

public class TaskDonatorsReset extends Task {
	protected static final Logger LOGGER = LoggerFactory.getLogger(TaskDonatorsReset.class.getName());
	public static final	String	NAME	= "donators_reset";

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public void onTimeElapsed(ExecutedTask task) {
		String playerName = "";
		Calendar cal = Calendar.getInstance();

		if((cal.get(Calendar.DAY_OF_WEEK) == Calendar.THURSDAY) || (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)) {
			try {	

				for (L2PcInstance player : L2World.getInstance().getAllPlayers()) {
					if (player.isDonator()) {
						player.setDonator(false);
					
					}
				}
			
			
				Connection con = null;
				try {
				
					con = L2DatabaseFactory.getInstance().getConnection();
					Statement g = con.createStatement();
					g.executeUpdate("UPDATE CharacterCustomData SET donator = 0"); 
					g.close();
				
				} catch (SQLException e) {
				} finally {
				try {
					if (con != null)
						con.close();
					} catch (SQLException e) {
					e.printStackTrace();
				}
				}
				LOGGER.info("Donators Reset Global Task: launched.");
			} catch (Exception e) {
				LOGGER.warn("Donators Reset Task Error : " + e);
			}
		}
	}

	@Override
	public void initializate() {
		super.initializate();
		TaskManager.addUniqueTask(NAME, TaskTypes.TYPE_GLOBAL_TASK, "1", "00:10:00", "");
	}
}