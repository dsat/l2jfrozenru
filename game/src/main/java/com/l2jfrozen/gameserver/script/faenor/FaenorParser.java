/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.script.faenor;

import com.l2jfrozen.gameserver.script.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.script.ScriptContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * @author Luis Arias
 */
public abstract class FaenorParser extends Parser {
    public final static boolean DEBUG = true;
    private static final Logger LOGGER = LoggerFactory.getLogger(FaenorParser.class.getName());
    protected static FaenorInterface _bridge = FaenorInterface.getInstance();
    protected final DateFormat DATE_FORMAT = new SimpleDateFormat("dd MMM yyyy", Locale.US);

    /*
     * UTILITY FUNCTIONS
     */
    public static String attribute(Node node, String attributeName) {
        return attribute(node, attributeName, null);
    }

    public static String element(Node node, String elementName) {
        return element(node, elementName, null);
    }

    public static String attribute(Node node, String attributeName, String defaultValue) {
        try {
            return node.getAttributes().getNamedItem(attributeName).getNodeValue();
        } catch (final Exception e) {
            LOGGER.error("", e);

            if (defaultValue != null)
                return defaultValue;
            throw new NullPointerException("FaenorParser: attribute " + e.getMessage());
        }
    }

    public static String element(Node parentNode, String elementName, String defaultValue) {
        try {
            final NodeList list = parentNode.getChildNodes();
            for (int i = 0; i < list.getLength(); i++) {
                final Node node = list.item(i);
                if (node.getNodeName().equalsIgnoreCase(elementName))
                    return node.getTextContent();
            }
        } catch (final Exception e) {
            LOGGER.error("", e);
        }
        if (defaultValue != null)
            return defaultValue;
        throw new NullPointerException();

    }

    public static boolean isNodeName(Node node, String name) {
        return node.getNodeName().equalsIgnoreCase(name);
    }

    public static double getPercent(String percent) {
        return Double.parseDouble(percent.split("%")[0]) / 100.0;
    }

    protected static int getInt(String number) {
        return Integer.parseInt(number);
    }

    protected static double getDouble(String number) {
        return Double.parseDouble(number);
    }

    protected static float getFloat(String number) {
        return Float.parseFloat(number);
    }

    protected static String getParserName(String name) {
        return "faenor.Faenor" + name + "Parser";
    }

    @Override
    public abstract void parseScript(Node node, ScriptContext context);
}
