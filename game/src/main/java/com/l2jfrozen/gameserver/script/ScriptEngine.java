/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.script;

import com.l2jfrozen.gameserver.script.faenor.FaenorInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Hashtable;

/**
 * @author Luis Arias
 */
public class ScriptEngine {
    public static final Hashtable<String, ParserFactory> parserFactories = new Hashtable<String, ParserFactory>();
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptEngine.class.getName());
    protected EngineInterface _utils = FaenorInterface.getInstance();

    protected static Parser createParser(String name) throws ParserNotCreatedException {
        ParserFactory s = parserFactories.get(name);
        if (s == null) // shape not found
        {
            try {
                Class.forName("com.l2jfrozen.gameserver.script." + name);
                // By now the static block with no function would
                // have been executed if the shape was found.
                // the shape is expected to have put its connection
                // in the hashtable.

                s = parserFactories.get(name);
                if (s == null)
                    throw new ParserNotCreatedException();
            } catch (final ClassNotFoundException e) {
                LOGGER.error("", e);

                // We'll throw an exception to indicate that
                // the shape could not be created
                throw new ParserNotCreatedException();
            }
        }
        return s.create();
    }
}
