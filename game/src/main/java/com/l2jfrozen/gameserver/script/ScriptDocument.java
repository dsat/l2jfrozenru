/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.script;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;

/**
 * L2JFrozen
 */
public class ScriptDocument {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScriptDocument.class.getName());
    private Document _document;
    private final String _name;

    public ScriptDocument(String name, InputStream input) {
        _name = name;

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            final DocumentBuilder builder = factory.newDocumentBuilder();
            _document = builder.parse(input);

        } catch (final SAXException sxe) {
            // Error generated during parsing)
            Exception x = sxe;
            if (sxe.getException() != null) {
                x = sxe.getException();
            }
            x.printStackTrace();

        } catch (final ParserConfigurationException pce) {
            // Parser with specified options can't be built
            pce.printStackTrace();
        } catch (final IOException ioe) {
            // I/O error
            ioe.printStackTrace();

        } finally {

            if (input != null)
                try {
                    input.close();
                } catch (final IOException e) {
                    LOGGER.error("unhandled exception", e);
                }
        }
    }

    public Document getDocument() {
        return _document;
    }

    /**
     * @return Returns the _name.
     */
    public String getName() {
        return _name;
    }

    @Override
    public String toString() {
        return _name;
    }

}
