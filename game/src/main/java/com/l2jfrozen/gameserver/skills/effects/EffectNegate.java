/*
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jfrozen.gameserver.skills.effects;

import com.l2jfrozen.configuration.GameServerConfig;
import com.l2jfrozen.gameserver.model.L2Effect;
import com.l2jfrozen.gameserver.model.L2Skill;
import com.l2jfrozen.gameserver.model.L2Skill.SkillType;
import com.l2jfrozen.gameserver.skills.Env;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author Gnat
 */
public class EffectNegate extends L2Effect {
    protected static final Logger LOGGER = LoggerFactory.getLogger(EffectNegate.class.getName());

    public EffectNegate(Env env, EffectTemplate template) {
        super(env, template);
    }

    @Override
    public EffectType getEffectType() {
        return EffectType.NEGATE;
    }

    @Override
    public void onStart() {

        final L2Skill skill = getSkill();

        if (GameServerConfig.DEBUG) {
            LOGGER.debug("effectNegate on " + getEffected().getName() + " with skill " + skill.getId());
        }

        if (skill.getNegateId() != 0) {
            getEffected().stopSkillEffects(skill.getNegateId());
        }

        for (final String negateSkillType : skill.getNegateSkillTypes()) {
            if (GameServerConfig.DEBUG) {
                LOGGER.debug("effectNegate on Type " + negateSkillType + " with power " + skill.getPower());
            }

            SkillType type = null;
            try {
                type = SkillType.valueOf(negateSkillType);
            } catch (final Exception e) {
                //
            }

            if (type != null) {
                getEffected().stopSkillEffects(type, skill.getPower());
            }
        }

        for (final String negateEffectType : skill.getNegateEffectTypes()) {
            if (GameServerConfig.DEBUG) {
                LOGGER.debug("effectNegate on Effect Type " + negateEffectType + " with power " + skill.getPower());
            }

            EffectType type = null;
            try {
                type = EffectType.valueOf(negateEffectType);
            } catch (final Exception e) {
                //
            }

            if (type != null) {
                getEffected().stopEffects(type);
            }
        }


    }


    @Override
    public boolean onActionTime() {
        return false;
    }
}
