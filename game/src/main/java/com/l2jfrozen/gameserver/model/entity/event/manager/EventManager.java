/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 *
 * http://www.gnu.org/copyleft/gpl.html
 */
package com.l2jfrozen.gameserver.model.entity.event.manager;

import com.l2jfrozen.configuration.ConfigManager;
import com.l2jfrozen.gameserver.model.entity.event.CTF;
import com.l2jfrozen.gameserver.model.entity.event.DM;
import com.l2jfrozen.gameserver.model.entity.event.TvT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

/**
 * @author Shyla
 */
@Component
public class EventManager {
    protected static final Logger LOGGER = LoggerFactory.getLogger(EventManager.class.getName());

    public static boolean TVT_EVENT_ENABLED;
    public static ArrayList<String> TVT_TIMES_LIST;
    @Autowired
    private ConfigManager configManager;
    public static boolean CTF_EVENT_ENABLED;
    public static ArrayList<String> CTF_TIMES_LIST;

    public static boolean DM_EVENT_ENABLED;
    public static ArrayList<String> DM_TIMES_LIST;

    private static EventManager instance = null;

    private EventManager() {
    }

    public static EventManager getInstance() {

        if (instance == null) {
            instance = new EventManager();
        }
        return instance;

    }

    public void startEventRegistration() {

        if (TVT_EVENT_ENABLED) {
            registerTvT();
        }

        if (CTF_EVENT_ENABLED) {
            registerCTF();
        }

        if (DM_EVENT_ENABLED) {
            registerDM();
        }

    }

    private static void registerTvT() {

        TvT.loadData();
        if (!TvT.checkStartJoinOk()) {
            LOGGER.info("registerTvT: TvT Event is not setted Properly");
        }

        //clear all tvt
        EventsGlobalTask.getInstance().clearEventTasksByEventName(TvT.get_eventName());

        for (final String time : TVT_TIMES_LIST) {

            final TvT newInstance = TvT.getNewInstance();
            //LOGGER.warn("SYS_LOG: "+"registerTvT: reg.time: "+time);
            newInstance.setEventStartTime(time);
            EventsGlobalTask.getInstance().registerNewEventTask(newInstance);

        }


    }

    private static void registerCTF() {

        CTF.loadData();
        if (!CTF.checkStartJoinOk()) {
            LOGGER.info("registerCTF: CTF Event is not setted Properly");
        }

        //clear all tvt
        EventsGlobalTask.getInstance().clearEventTasksByEventName(CTF.get_eventName());


        for (final String time : CTF_TIMES_LIST) {

            final CTF newInstance = CTF.getNewInstance();
            //LOGGER.warn("SYS_LOG: "+"registerCTF: reg.time: "+time);
            newInstance.setEventStartTime(time);
            EventsGlobalTask.getInstance().registerNewEventTask(newInstance);

        }

    }

    private static void registerDM() {
        DM.loadData();
        if (!DM.checkStartJoinOk()) {
            LOGGER.info("registerDM: DM Event is not setted Properly");
        }

        //clear all tvt
        EventsGlobalTask.getInstance().clearEventTasksByEventName(DM.get_eventName());


        for (final String time : DM_TIMES_LIST) {

            final DM newInstance = DM.getNewInstance();
            //LOGGER.warn("SYS_LOG: "+"registerDM: reg.time: "+time);
            newInstance.setEventStartTime(time);
            EventsGlobalTask.getInstance().registerNewEventTask(newInstance);

        }
    }

    @PostConstruct
    public void load() {
        try {

            //============================================================

            TVT_EVENT_ENABLED = configManager.getBoolean("TVTEventEnabled");
            TVT_TIMES_LIST = new ArrayList<String>();

            String[] propertySplit;
            propertySplit = configManager.getString("TVTStartTime").split(";");

            for (final String time : propertySplit) {
                TVT_TIMES_LIST.add(time);
            }

            CTF_EVENT_ENABLED = configManager.getBoolean("CTFEventEnabled");
            CTF_TIMES_LIST = new ArrayList<String>();

            propertySplit = configManager.getString("CTFStartTime").split(";");

            for (final String time : propertySplit) {
                CTF_TIMES_LIST.add(time);
            }

            DM_EVENT_ENABLED = configManager.getBoolean("DMEventEnabled");
            DM_TIMES_LIST = new ArrayList<String>();

            propertySplit = configManager.getString("DMStartTime").split(";");

            for (final String time : propertySplit) {
                DM_TIMES_LIST.add(time);
            }

        } catch (final Exception e) {
            e.printStackTrace();

        }
    }
}
